// Kasama na ata ito sa DOM manupulation. Dko lang sure. Terms. Hahaha.

const logOut = document.getElementById("logout");
logOut.addEventListener("click", () => loggedOut());

//--------------------------------------------------------------

// Dito naman is, pag nag logout sila, diretcho alis ang token na
// gamit nila para sa authentication nila. Tapos maredirect sila sa login page at
// hindi nila masisilip ang dashBoard since .replace ang gamit.
// Search niyo na lang mga sir si location.replace().
// May mas magandang paraan yan pag dating sa Reactjs natin. Hehe.
const loggedOut = () => {
  localStorage.removeItem("token");
  location.replace("../HTML/login.html");
};

// Ito naman ay parang pang double secure lang.
// Hindi nila makikita ang user page kasi naka set sa sa diaply none
// si body ni dashBoard html.
const isAuthenticated = () => {
  const token = localStorage.getItem("token");
  if (!token) {
    document.body.style.display = "none";
    loggedOut();
    return;
  }
  document.body.style.display = "block";
};

isAuthenticated();
