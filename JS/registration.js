// Kasama na ata ito sa DOM manupulation. Dko lang sure. Terms. Hahaha.
const email = document.getElementById("email");
const userName = document.getElementById("user-name");
const password = document.getElementById("password");
const confirmPassword = document.getElementById("confirm-password");
const submitButton = document.getElementById("submit-button");

email.addEventListener("input", () => confirmPasswordHandler());
userName.addEventListener("input", () => confirmPasswordHandler());
password.addEventListener("input", () => confirmPasswordHandler());
confirmPassword.addEventListener("input", () => confirmPasswordHandler());
submitButton.addEventListener("click", () => credentialsHandler());

//------------------------------------------------------------------------

// Local Storage muna, mga sirs. Lipat na lang natin pag marunong na tayo sa API. Hehe.
// Di ko na muna linagyan ng custom "checkers" (dko alam term. haha) ang kada input field
// since mapapahaba masyado ang basic code.
const saveToStorage = (credentails) => {
  // Dito pumasok si destrcuturing. If hindi nyo pa mga sir alam ang ...varbile, 
  // search nyo na lang ang ...rest JS MDN. Kukuha nyo yan agad. Kayo pa. Hehe.
  const { email, ...restOfCreds } = credentails;
  localStorage.setItem(credentails.email, JSON.stringify(restOfCreds));
};

const accountChecker = (credentails) => {
  const emailExists = localStorage.getItem(credentails.email);
  if (emailExists) {
    console.log("Email is already in use");
    return;
  }
  saveToStorage(credentails);
};

const credentialsHandler = () => {
  const credentails = {
    // Sa id na key, temporary lang, parang dummy id muna. Hehe.
    id: Math.floor(Math.random() * Math.random() * new Date()),
    email: email.value,
    userName: userName.value,
    password: password.value,
  };
  accountChecker(credentails);
};

// Pang check lang if filled and input boxes para maenable ang registration button
const confirmPasswordHandler = () => {
  if (
    !email.value ||
    !userName.value ||
    !password ||
    password.value !== confirmPassword.value
  ) {
    submitButton.setAttribute("disabled", "");
    return;
  }
  submitButton.removeAttribute("disabled");
};
