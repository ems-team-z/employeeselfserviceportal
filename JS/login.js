// Kasama na ata ito sa DOM manupulation. Dko lang sure. Terms. Hahaha.
const userName = document.getElementById("user_user-name");
const password = document.getElementById("user_password");
const loginButton = document.getElementById("login-button");

userName.addEventListener("input", () => loginCredChecker());
password.addEventListener("input", () => loginCredChecker());
loginButton.addEventListener("click", () => loginHandler());
//---------------------------------------------------------------------

// If my questions dito, mga sir, kung pano at anong ginawa.
// Sabihan nyo lang ako. Mag meeting ulit tayo at your most convenient time.
// Mahaba kasi kapag i-comment dito. Haha.
// Pero alam ko kaya nyo yan magets. Hehe.
const loginHandler = () => {
  const isRegistered = localStorage.getItem(userName.value);
  const convertedJSON = JSON.parse(isRegistered);
  if (!convertedJSON) {
    console.log("Email not registered");
    return;
  }
  if (password.value !== convertedJSON.password) {
    console.log("Incorrect Username or Password");
    return;
  }
  // Next lines is para mag set ng token para sa authentication. Siguro pag sa API natin,
  // separate na collection ang storage ng token. Parang kasi isama sa collection ng registered users.
  // Also dapat one is to one is to one lang ang token. One account per token per login.
  // Where ang token ay nag babago every login.
  location.replace("../HTML/dashBoard.html");
  localStorage.setItem("token", convertedJSON.id);
};

// Pang check lang if filled and input boxes para maenable ang login button
const loginCredChecker = () => {
  if (!userName.value || !password.value) {
    loginButton.setAttribute("disabled", "");
    return;
  }
  loginButton.removeAttribute("disabled");
};
